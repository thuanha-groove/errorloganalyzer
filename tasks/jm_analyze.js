'use strict';

var EOL = require('os').EOL;

module.exports = function(grunt) {
    grunt.registerTask('downloadlogfiles', 'Download Error Logs', function() {
        var done = this.async();
        downloadlogfiles(grunt.config('analyzelogfiles.options'), function() {
            done();
        });
    });

    grunt.registerTask('analyzelogfiles', 'Analyze Error Logs', function() {
        analyze(grunt.config('analyzelogfiles.options'));
    });

    /**
     * Measures the size (in kb) of the error log files.
     * A CSV file named size.csv will contain the total size.
     */
    grunt.registerTask('measuresizelogfiles', 'Measure Error Logs Size', function() {
        var done = this.async(),
            options = grunt.config('analyzelogfiles.options');
        getErrorLogFileList(options, function(fileEntries) {
            var totalSize = 0.0;

            // loop through file entries and aggregate file size
            fileEntries.forEach(function(fileEntry) {
                totalSize += fileEntry.size;
            });

            writeFile(options, "size.csv", "Total Log Size" + EOL + totalSize + EOL);
            done();
        });
    });
}

function downloadlogfiles(options, callback) {
    getErrorLogFileList(options, function(fileEntries) {
        if (!fileEntries || fileEntries.length <= 0) {
            console.log("No log files to download");
            callback();
            return;
        }

        var complete = [];
        fileEntries.forEach(function(fileEntry) {
            var file = fileEntry.name;
            getErrorLog(options, file, function(fileData) {
                console.log('Downloading log: ' + file);
                writeFile(options, file, fileData);
                if (complete.length == fileEntries.length - 1) {
                    callback();
                } else {
                    complete.push(true);
                }
            }), function (e) {
                complete.push(true);
            }
        })
    });
}

function analyze(options) {
    var logParser = LogParser(options.group_errors);
    var fs = require('fs');
    var path = require('path');
    var files = fs.readdirSync(options.errorlog_dir);

    files.forEach(function (file) {
        console.log('analyzing: ' + file);
        var fileData = fs.readFileSync(path.join(options.errorlog_dir, file), {encoding: 'utf8'});
        logParser.parse(file, fileData);
    });

    fs.readFile('template/json_formatter.js', "utf8", function(err, json_formatter) {
        writeFile(options, 'summary.html', logParser.summary(options, json_formatter));
    });

    writeFile(options, 'summary.csv', logParser.csvSummary(options));
}

/**
 * Gets the file list of error log files for a given date and passes them to the callback function.
 *
 * The callback function will receive an array of file entry objects. Each file entry object
 * contains two properties: name (file name without path) and size (file size in kb).
 */
function getErrorLogFileList(options, callback) {
    var https = require('https');
    var dateFilter = getDate(options.log_date);
    console.log('options', options);

    var req = https.get({
        rejectUnauthorized: false,
        host: options.webdav_server,
        path: '/on/demandware.servlet/webdav/Sites/Logs',
        auth: options.webdav_username + ":" + options.webdav_password
    }, function(resp) {
        var str = "";

        resp.on('data', function(data) {
            str += data;
        });

        resp.on('end', function() {
            if (str.indexOf('401 - Authorization Required') !== -1) {
                throw '401 - Authorization Required';
            }

            var fileEntries = [],
                re = /a href="[^"]*\/([a-z\-]*error[a-z0-9\-]+\.log|quota-blade[a-z0-9\-]+\.log)"[\s\S]+?([0-9\.]+) kb/g,
                matches = [],
                match;

            do {
                match = re.exec(str);
                if (match) {
                    matches.push(match);
                }
            } while (match);

            if (matches.length) {
                matches.forEach(function(m) {
                    var fileName = m[1];
                    if (fileName.indexOf(dateFilter) !== -1) {
                        fileEntries.push({
                            name: fileName,
                            size: parseFloat(m[2])
                        });
                    }
                });
            }

            callback(fileEntries);
        });
    });

    req.on('error', function(e) {
        console.error(e);
    });
};

function getErrorLog(options, filename, callback) {
    var https = require('https');

    var req = https.get({
        rejectUnauthorized: false,
        host: options.webdav_server,
        path: '/on/demandware.servlet/webdav/Sites/Logs/' + filename,
        auth: options.webdav_username + ":" + options.webdav_password
    }, function(resp) {
        var str = "";
        resp.on('data', function(chunk) {
            str += chunk;
        });
        resp.on('end', function() {
            callback(str);
        })
    });

    req.on('error', function(e) {
        console.error(e);
        callback(e);
    });
};

function writeFile(options, file, fileData) {
    var path = require("path");
    var fs = require("fs");
    fs.writeFileSync(path.join(options.errorlog_dir, file), fileData);
}

function LogParser(groupErrors) {
    var totalErrors = [];
    var MAX_ERRORS = 500;

    // Parse a log file and return an array of error objects
    function parseLog(fileName, contents) {
        var errorRegex = /^\[([^\]]*)\]([^\|]*)\|([^\|]*)\|([^\|]*)\|([^\|]*)\|([^\|]*)\|(.*)$/gm,
            match, matches = [],
            errors = [],
            normalizedErrors = [],
            isQuotaFile = fileName.indexOf("quota") === 0;

        do {
            match = errorRegex.exec(contents);
            match && matches.push(match);
        } while (match);

        if (matches.length) {
            matches.forEach(function(m) {
                var website = m[4].trim(),
                    pipeline = m[5].trim(),
                    errorMessage = m[7].trim().replace(/\r/g, ""),
                    sessionid = m[3].trim();

                if (website.substr(0, 6).toLowerCase() === "sites-") {
                    errors.push({
                        'index': m.index,
                        'match': m[0],
                        'errorDate': m[1],
                        'errorSource': m[2].trim(),
                        'sessionid': sessionid,
                        'website': website,
                        'pipeline': pipeline,
                        'errorMessage': errorMessage
                    });
                } else {
                    errors.push({
                        'index': m.index,
                        'match': m[0],
                        'errorDate': m[1],
                        'errorSource': m[2].trim(),
                        'sessionid': '',
                        'website': 'System',
                        'pipeline': '---',
                        'errorMessage': m[0]
                    });
                };
            });

            errors.forEach(function(error, index) {
                var start, length;
                start = error.index + error.match.length;
                length = 0;

                if ((index + 1) < errors.length) {
                    length = errors[index + 1].index - start;
                } else {
                    length = contents.length;
                };

                error.errorDetail = contents.substr(start, length).trim().replace(/\r$/g, "");
                var findErroMessage = /(?:.*? ){9}(.*)/.exec(error.errorMessage);
                error.errorKey = findErroMessage ? findErroMessage[1] : error.errorMessage;

                if (error.website === 'System') {
                    var end = error.errorKey.search(/\[\d/);
                    if (end > 0) {
                        error.errorKey = error.errorKey.substring(0, end-1);
                    }
                }

                /*if (error.errorKey.length > 200) {
                    error.errorKey = error.errorKey.substring(0, 200);
                }*/

                var isInternalQuotaError = error.errorKey.indexOf("(internal") !== -1
                if (isQuotaFile && isInternalQuotaError) {
                    delete errors[index];
                }
            });

            errors = errors.filter(function (error) {
                return !!error;
            })

            // This logic optionally groups the errors that are similiar to each other by making the
            // error key for errors that are identical but have 1 difference, in this way
            // the errors are grouped together in the error report and makes the error report smaller
            // and easier to read
            if (groupErrors) {
                errors.forEach(function(e, index) {
                    var errorParts = e.errorKey.split(" ");

                    for (var i = 0; i < errors.length; i++) {
                        if (i !== index) {
                            var differences = 0;
                            var normalizedParts = errors[i].errorKey.split(" ");
                            if (errorParts.length === normalizedParts.length) {
                                for (var j = 0; j < errorParts.length; j++) {
                                    if (errorParts[j] !== normalizedParts[j]) {
                                        differences++;
                                    }
                                }

                                if (differences === 1) {
                                    for (var k = 0; k < errorParts.length; k++) {
                                        if (errorParts[k] !== normalizedParts[k]) {
                                            errorParts[k] = "---";
                                            normalizedParts[k] = "---";
                                        }
                                    };
                                    e.errorKey = errorParts.join(" ");
                                    errors[i].errorKey = normalizedParts.join(" ");
                                }
                            }
                        }
                    }
                });
            }
        };

        return errors;
    };

    // Generates the "summary" data, which consists of the total errors per website and for each
    // website the total errors per pipeline
    function summarizeErrorsByWebSiteInJson(errors) {
        var finalJson = {
            errorTotals: [],
            errorDetail: []
        };
        var sortedErrors, summary = "",
            prevWebSite, countWebSite, totalErrorCount, results = [];

        sortedErrors = errors.sortObjects("website");
        prevWebSite = sortedErrors[0] ? sortedErrors[0].website : "System";
        countWebSite = 0;
        totalErrorCount = 0;

        sortedErrors.forEach(function(ele) {
            if (ele.website !== prevWebSite) {
                results.push({
                    "website": prevWebSite,
                    "count": countWebSite
                });
                totalErrorCount += countWebSite;
                countWebSite = 1;
                prevWebSite = ele.website;
            } else {
                countWebSite++;
            };
        });

        totalErrorCount += countWebSite;
        results.push({
            "website": sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].website : "System",
            "count": countWebSite
        });

        //Sort websites by error count descending
        results = results.sortObjects("count", true);

        results.forEach(function(ele) {
            var currWebSite = ele.website;
            var siteObj = {
                count: ele.count,
                website: ele.website,
                isMaxErrors: ele.count > MAX_ERRORS ? true : false
            };
            finalJson.errorTotals.push(siteObj);

            var cloneSiteObj = JSON.parse(JSON.stringify(siteObj));
            cloneSiteObj.pipelines = summarizeErrorsByPipelineInJson(sortedErrors.filter(function(ele) {
                return ele.website === currWebSite;
            })) || [];

            finalJson.errorDetail.push(cloneSiteObj);
        });

        return JSON.stringify(finalJson);
    };

    // This function is called by the "summarizeErrorsByWebSite" function to produce the list of
    // pipelines for a specific website and the count of errors for each pipeline.
    // The "errors" parameter is assumed to be a subset of the "errors" array produced by filtering
    // on the "website" property.
    function summarizeErrorsByPipelineInJson(errors) {
        var pipelinesArray = [];
        var sortedErrors, summary = "",
            prevPipeline, countPipeline, results = [];

        sortedErrors = errors.sortObjects("pipeline");
        prevPipeline = sortedErrors[0] ? sortedErrors[0].pipeline : "System";
        countPipeline = 0;

        sortedErrors.forEach(function(ele) {
            if (ele.pipeline !== prevPipeline) {
                results.push({
                    "pipeline": prevPipeline,
                    "count": countPipeline
                });
                countPipeline = 1;
                prevPipeline = ele.pipeline;
            } else {
                countPipeline++;
            };
        });

        results.push({
            "pipeline": sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].pipeline : "System",
            "count": countPipeline
        });

        //Sort pipelines by error count descending
        results = results.sortObjects("count", true);

        results.forEach(function(ele) {
            var currPipeline = ele.pipeline;
            ele.errorsKey = summarizeErrorsByErrorKeyInJson(sortedErrors.filter(function(ele) {
                return ele.pipeline === currPipeline;
            })) || [];
            pipelinesArray.push(ele);
        });

        return pipelinesArray;
    };

    // This function is called by the "summarizeErrorsByWebSite" function to produce the list of
    // pipelines for a specific website and the count of errors for each pipeline.
    // The "errors" parameter is assumed to be a subset of the "errors" array produced by filtering
    // on the "website" property.
    function summarizeErrorsByErrorKeyInJson(errors) {
        var sortedErrors, summary = "",
            prevError, lastError, countError, results = [];

        sortedErrors = errors.sortObjects("errorKey");
        prevError = sortedErrors[0] ? sortedErrors[0].errorKey : "System";
        countError = 0;
        var timeOccur = [];
        sortedErrors.forEach(function(ele) {
            var currError = ele.errorKey;
            if(!timeOccur[currError])
                timeOccur[currError] = [ele.errorDate];
            else
                timeOccur[currError].push(ele.errorDate);
            if (currError !== prevError) {
                results.push({
                    "errorMessage": prevError,
                    "count": countError,
                    "timeOfOccurrence": timeOccur[prevError].length > 1 ? (timeOccur[prevError][timeOccur[prevError].length-1] + ' -> ' +timeOccur[prevError][0]) : timeOccur[prevError][0]
                });
                countError = 1;
                prevError = currError;
            } else {
                countError++;
            };
        });

        lastError = sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].errorKey : "System";
        results.push({
            "errorMessage": lastError,
            "count": countError,
            "timeOfOccurrence": timeOccur[lastError].length > 1 ? (timeOccur[lastError][timeOccur[lastError].length-1] + ' -> ' +timeOccur[lastError][0]) : timeOccur[lastError][0]
        });

        //Sort errors by error count descending
        results = results.sortObjects("count", true);
        return results;
    };

    return {
        "parse": function(fileName, fileContents) {
            var errors = parseLog(fileName, fileContents);
            errors.forEach(function(ele) {
                totalErrors.push(ele);
            });
            return errors.length;
        },
        "summary": function(options, json_formatter) {
            var summary = "";
            summary += "<html>";
            summary += "<head>";
            summary += '<style>.jsonview{font-family:monospace;font-size:1.1em;white-space:pre-wrap}.jsonview .prop{font-weight:700;text-decoration:none;color:#000}.jsonview .null,.jsonview .undefined{color:red}.jsonview .bool,.jsonview .num{color:#00f}.jsonview .string{color:green;white-space:pre-wrap}.jsonview .string.multiline{display:inline-block;vertical-align:text-top}.jsonview .collapser{position:absolute;left:-1em;cursor:pointer}.jsonview .collapsible{transition:height 1.2s;transition:width 1.2s}.jsonview .collapsible.collapsed{height:.8em;width:1em;display:inline-block;overflow:hidden;margin:0}.jsonview .collapsible.collapsed:before{content:"…";width:1em;margin-left:.2em}.jsonview .collapser.collapsed{transform:rotate(0)}.jsonview .q{display:inline-block;width:0;color:transparent}.jsonview li{position:relative}.jsonview ul{list-style:none;margin:0 0 0 2em;padding:0}.jsonview h1{font-size:1.2em}</style>';
            summary += "<script>"+json_formatter+"</script>";
            summary += "</head><body>";
            summary += "Server: <strong>" + options.webdav_server + "</strong><br/>";
            summary += "Log Date: <strong>" + getDateForDisplay(options.log_date) + "</strong><br/>";
            summary += "Total Errors: <strong>" + totalErrors.length + "</strong><br/>";
            summary += "<div id='json'></div>";
            summary += "<script> var summaryJson=" + summarizeErrorsByWebSiteInJson(totalErrors) + ";";
            summary += "$('#json').JSONView(summaryJson);";
            summary += "</script>";
            summary += "</body></html>";
            return summary;
        },
        /**
         * Generate CSV summary.
         * Currently it only puts one row and one column per site.
         */
        "csvSummary": function(options) {
            var csvSummary = "",
                sortedErrors,
                prevWebSite,
                countWebSite = 0,
                totalErrorCount = 0,
                results = [];

            sortedErrors = totalErrors.sortObjects("website");
            prevWebSite = sortedErrors[0] ? sortedErrors[0].website : "System";

            sortedErrors.forEach(function(ele) {
                if (ele.website !== prevWebSite) {
                    results.push({
                        "website": prevWebSite,
                        "count": countWebSite
                    });

                    totalErrorCount += countWebSite;
                    countWebSite = 1;
                    prevWebSite = ele.website;
                } else {
                    countWebSite++;
                }
            });

            totalErrorCount += countWebSite;

            results.push({
                "website": sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].website : "System",
                "count": countWebSite
            });

            results.unshift({
                "website" : "Total",
                "count": totalErrorCount
            });

            for (var i = 0; i < results.length; i++) {
                if (i > 0) {
                    csvSummary += ",";
                }

                csvSummary += results[i].website;
            }

            csvSummary += EOL;

            for (var i = 0; i < results.length; i++) {
                if (i > 0) {
                    csvSummary += ",";
                }

                csvSummary += results[i].count.toString();
            }

            csvSummary += EOL;
            return csvSummary;
        }
    };
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&apos;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function getDate(d) {
    var today = d ? new Date(d) : new Date(),
        year = today.getFullYear(),
        month = today.getMonth() + 1,
        day = today.getDate();

    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    return year + "" + month + "" + day;
}

function getDateForDisplay(d) {
    var today = d ? new Date(d) : new Date(),
        year = today.getFullYear(),
        month = today.getMonth() + 1,
        day = today.getDate();

    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    return month + '/' + day + '/' + year ;
}

if (typeof Array.prototype.sortObjects === "undefined") {
    Array.prototype.sortObjects = function(property, desc) {
        // Sorts an array of objects based on a specific object property
        var returnArray = this.slice(0);

        returnArray.sort(function(p1, p2) {
            var returnValue,
                first = p1[property],
                second = p2[property];

            if (first === second) {
                returnValue = 0;
            } else if (first < second) {
                returnValue = -1;
            } else {
                returnValue = 1;
            };
            return desc ? (returnValue * -1) : returnValue;
        });

        return returnArray;
    };
};

if (typeof String.prototype.trim === "undefined") {
    String.prototype.trim = function() {
        // Trim whitespace from beginning and end of the string
        return this.replace(/^\s*/, "").replace(/\s*$/, "");
    };
};
