'use strict';

module.exports = function(grunt) {
    require('time-grunt')(grunt);

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks("grunt-nodemailer");
    grunt.loadNpmTasks('grunt-prompt');

    /*
    Below is an explanation of the parameters/options used in this task.
        For more information on options see: http://gruntjs.com/api/grunt.option

    --ANALYZE_LOG_DIR
        Folder where log files are downloaded to on your computer.
        The ZIP and summary.htm are created in this folder.
    --ANALYZE_LOG_DATE
        The date to run the report for. You can specify any valid javascript date string.
        OPTIONAL: defaults to the current date.
    --WEBDAV_SERVER
        The server to download the log files from.
    --WEBDAV_USERNAME
        Username for the WebDav server
    --WEBDAV_PASSWORD
        Password for the WebDav server
    --EMAIL_USERNAME
        The email username to use if you want to email the ZIP and summary.htm.
        OPTIONAL: if not specified then no email is sent
    --EMAIL_PASSWORD
        The email password to use if you want to email the ZIP and summary.htm.
        OPTIONAL: if not specified then no email is sent
    --EMAIL_RECIPIENT
        The email recipient to use if you want to email the ZIP and summary.htm.
        OPTIONAL: if not specified then no email is sent

    Modify the 'default' variables velow if you intend to run this grunt task without
    parameters.
    */

    var default_webdav_server = '';
    var default_webdav_username = '';
    var default_webdav_password = '';
    var default_log_dir = '';
    var default_log_date = new Date();
    var default_email_user = '';
    var default_email_password = '';
    var default_email_recipient = '';

    var webdav_server = grunt.option('WEBDAV_SERVER') || default_webdav_server;
    var webdav_username = grunt.option('WEBDAV_USERNAME') || default_webdav_username;
    var webdav_password = grunt.option('WEBDAV_PASSWORD') || default_webdav_password;
    var log_dir = grunt.option('ANALYZE_LOG_DIR') || default_log_dir;
    var log_date = grunt.option('ANALYZE_LOG_DATE') || default_log_date;
    var email_username = grunt.option('EMAIL_USERNAME') || default_email_user;
    var email_password = grunt.option('EMAIL_PASSWORD') || default_email_password;
    var email_recipient = grunt.option('EMAIL_RECIPIENT') || default_email_recipient;


    var persistConfig = {};
    try{
        persistConfig = grunt.file.readJSON('config.json');
    } catch(e) {}
    if(persistConfig && persistConfig.sandboxInfo && persistConfig.sandboxInfo.domain && persistConfig.sandboxInfo.username && persistConfig.sandboxInfo.password && persistConfig.sandboxInfo.output
            && persistConfig.sandboxInfo.domain.length > 0 && persistConfig.sandboxInfo.username.length > 0 && persistConfig.sandboxInfo.password.length > 0 && persistConfig.sandboxInfo.output.length > 0) {
        webdav_server = persistConfig.sandboxInfo.domain;
        webdav_username = persistConfig.sandboxInfo.username;
        webdav_password = persistConfig.sandboxInfo.password;
        log_dir = persistConfig.sandboxInfo.output;
    }
    

    var path = require('path');

    grunt.initConfig({
        prompt: {
            usePersistConfig: {
                options: {
                    questions: [{
                        config: 'useNewConfig',
                        type: 'confirm',
                        default: false,
                        message: 'You had configured the sandbox information. Do you want to configure other?',
                    }],
                    then: function(results) {
                        console.log('useNewConfig', grunt.config('useNewConfig'));
                        if(grunt.config('useNewConfig')) {
                            grunt.task.run(['prompt:sandboxInfo', 'analyze']);
                        } else {
                            grunt.task.run(['analyze']);
                        }
                    }
                }
            },
            sandboxInfo: {
                options: {
                    questions: [{
                        config: 'sandboxInfo.domain',
                        type: 'input',
                        message: 'What is your sandbox domain?',
                    }, {
                        config: 'sandboxInfo.username',
                        type: 'input',
                        message: 'Webdav username?',
                    },
                    {
                        config: 'sandboxInfo.password',
                        type: 'password',
                        message: 'Password?',
                    },
                    {
                        config: 'sandboxInfo.output',
                        type: 'input',
                        message: 'What is output folder?',
                    }],
                    then: function(results) {
                        grunt.config.set('analyzelogfiles.options.webdav_server', grunt.config('sandboxInfo.domain'));
                        grunt.config.set('analyzelogfiles.options.webdav_username', grunt.config('sandboxInfo.username'));
                        grunt.config.set('analyzelogfiles.options.webdav_password', grunt.config('sandboxInfo.password'));

                        var answered_log_dir = grunt.config('sandboxInfo.output');
                        grunt.config.set('analyzelogfiles.options.errorlog_dir', answered_log_dir);
                        grunt.config.set('clean.analyze.src', answered_log_dir);
                        grunt.config.set('mkdir.analyze.options.create', [answered_log_dir]);
                        grunt.config.set('compress.analyze.options.archive', path.join(answered_log_dir, 'logs.zip'));
                        grunt.config.set('compress.analyze.files', [{
                            src: ['**/*.log'],
                            cwd: answered_log_dir,
                            expand: true
                        }]);
                        grunt.config.set('nodemailer.analyze.options.message.attachments', [
                        {
                            filename: 'logs.zip',
                            encoding: 'binary',
                            filePath: path.join(answered_log_dir, 'logs.zip')
                        }]);
                        grunt.config.set('nodemailer.analyze.src', [path.join(answered_log_dir, 'summary.htm')]);

                        var fs = require("fs");
                        fs.writeFile('config.json', JSON.stringify({
                            "sandboxInfo": {
                                "domain" : grunt.config('sandboxInfo.domain'),
                                "username" : grunt.config('sandboxInfo.username'),
                                "password" : grunt.config('sandboxInfo.password'),
                                "output" : grunt.config('sandboxInfo.output'),
                            }
                        }));
                    }
                }
            }
        },
        clean: {
            analyze: {
                options: {
                    force: true
                },
                src: log_dir
            }
        },
        mkdir: {
            analyze: {
                options: {
                    create: [log_dir]
                }
            }
        },
        compress: {
            analyze: {
                options: {
                    archive: path.join(log_dir, 'logs.zip'),
                    mode: 'zip'
                },
                files: [{
                    src: ['**/*.log'],
                    cwd: log_dir,
                    expand: true
                }]
            }
        },
        nodemailer: {
            analyze: {
                options: {
                    transport: {
                        type: 'SMTP',
                        options: {
                            service: 'Gmail',
                            auth: {
                                user: email_username,
                                pass: email_password
                            }
                        }
                    },
                    message: {
                        from: 'ErrorLogAnalyzer',
                        to: email_recipient,
                        subject: 'Error Log Summary',
                        attachments: [
                        {
                            filename: 'logs.zip',
                            encoding: 'binary',
                            filePath: path.join(log_dir, 'logs.zip')
                        }
                    ]
                    }
                },
                src: [path.join(log_dir, 'summary.htm')]
            }
        },
        analyzelogfiles: {
            options: {
                webdav_server: webdav_server,
                webdav_username: webdav_username,
                webdav_password: webdav_password,
                log_date: log_date,
                errorlog_dir: log_dir,
                group_errors: true,
            }
        }
    });

    grunt.loadTasks("./tasks");

    if (email_recipient && email_password && email_username) {
        grunt.registerTask('analyze', ['clean:analyze', 'mkdir:analyze', 'downloadlogfiles', 'analyzelogfiles', 'compress:analyze', 'nodemailer:analyze']);
    } else {
        grunt.registerTask('analyze', ['clean:analyze', 'mkdir:analyze', 'downloadlogfiles', 'analyzelogfiles', 'compress:analyze']);
    }

    if(persistConfig && persistConfig.sandboxInfo && persistConfig.sandboxInfo.domain && persistConfig.sandboxInfo.username && persistConfig.sandboxInfo.password && persistConfig.sandboxInfo.output
        && persistConfig.sandboxInfo.domain.length > 0 && persistConfig.sandboxInfo.username.length > 0 && persistConfig.sandboxInfo.password.length > 0 && persistConfig.sandboxInfo.output.length > 0)
        grunt.registerTask('default', ['prompt:usePersistConfig']);
    else
        grunt.registerTask('default', ['prompt:sandboxInfo', 'analyze']);

    /**
     * Measures the size (in kb) of the log files of a given date.
     * The output is written as a CSV file.
     * All standard options of analyze task apply.
     */
    grunt.registerTask('measuresize', ['clean:analyze', 'mkdir:analyze', 'measuresizelogfiles']);
};
