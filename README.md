##Overview
This repository forked from https://bitbucket.org/jmatos/grunttasks/ repository.

Hopefully, this tool will help you to save much time to control the log file. 

I really appreciate your contributions to improve this tool :)


## Changelogs
1. Applied quicker pickup sandbox which needs to be analyzed the logs, allowed utilizing the previous configuration or setting up a new configuration.
2. Refactored the logic to get exactly the error message, add more extra fields e.g. time of occurrence. 
3. Applied JSON viewer to be collapsed/expanded 



##Requirements
1. [Git](https://git-scm.com/downloads)
2. [Node.js](https://nodejs.org/en/)

##Getting started
Clone the repository   
```sh
$ git clone git@bitbucket.org:thuanha-amblique/errorloganalyzer.git
```

Install dependencies  
```sh
$ npm install
```

Install gulp globally  
```sh
$ npm install -g grunt
```

Start the analyzer
```sh
$ grunt
```